#!/usr/bin/env python3
# *-* coding: utf-8 *-*

# ─── MODULE INFORMATIONS ─────────────────────────────────────────────────────

# This module is part of the moodleDisordBot project

__author__ = "Etienne Le Lay"
__version__ = 0.1

# ─────────────────────────────────────────────────────────────────────────────


# ─── LIBRAIRIES IMPORT ───────────────────────────────────────────────────────

from ics import Calendar, Event
import requests as req
from tools import date_translate, colors
from datetime import datetime
import locale

# ─────────────────────────────────────────────────────────────────────────────

# ─── RETURN EVENTS OF SPECIFIC MONTH ─────────────────────────────────────────


def get_frama_events(months=None, year=None, course_name=None):
    """
    Parse events from a remote calendar.
    """
    # Set local to FR
    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

    # Some variables
    events = {}
    event_id = 0
    today = datetime.today().strftime("%d-%m-%Y")
    list_dates = []
    dict_events = {}

    # Read the framagenda from export url
    frama_url = "https://framagenda.org/remote.php/dav/public-calendars/iCYTe4STrZ7nn4dH/?export"
    ics_data = req.get(frama_url).content.decode("utf-8")
    c = Calendar(ics_data)

    # Case if we request for course infos
    if course_name is not None:
        month = today.split("-")[1]
        year = today.split("-")[2]
        pattern = f"{year}-{month}"
        print(
            f"[{colors.blue('*')}] Requesting every event for course : {course_name}")
        # We need to create a list of request month's dates
        for _event in c.events:
            event_name = str(_event.name)
            if course_name in event_name and pattern in str(_event.begin):
                list_dates.append(str(_event.begin))

        list_dates = sorted(list_dates)  # Sorting dates from recent to last

       # Re-order events by date
        for _event_date in list_dates:
            for _event in c.events:
                if _event_date == str(_event.begin):
                    event_id += 1
                    event_start = date_translate.ymdthms_to_dmyhm(
                        str(_event.begin).split("+")[0]
                    )
                    event_stop = date_translate.ymdthms_to_dmyhm(
                        str(_event.end).split("+")[0]
                    )
                    event_location = _event.location
                    event_description = _event.description
                    event_start_date = event_start.split(year)[0]
                    event_start_hour = event_start.split(year)[1]
                    event_stop_hour = event_stop.split(year)[1]
                    events[event_id] = {
                        "timestamp": _event.begin,
                        "start_date": event_start_date,
                        "start_hour": event_start_hour,
                        "stop_hour": event_stop_hour,
                        "description": event_description,
                        "location": event_location,
                    }
        event_count = len(events)
        return events, event_count

    # Request for events of the month
    if len(months) != 0 and year is not None and course_name is None:
        event_count = 0
        events = {}
        for month in months:
            pattern = f"{year}-{month}"
            print(f"[{colors.blue('*')}] Requesting events of {pattern}")

            # We need to create a list of request month's dates
            for _event in c.events:
                if pattern in str(_event.begin):
                    list_dates.append(str(_event.begin))

            # Sorting dates from recent to last
            list_dates = sorted(list_dates)
            # Re-order events by date
            for date in list_dates:
                for _event in c.events:
                    if date == str(_event.begin):
                        events[date] = {}
                        event_start = date_translate.ymdthms_to_dmyhm(
                            str(_event.begin).split("+")[0]
                        )
                        event_stop = date_translate.ymdthms_to_dmyhm(
                            str(_event.end).split("+")[0]
                        )
                        event_start_date = event_start.split(year)[0] + year
                        event_start_hour = event_start.split(year)[1]
                        event_stop_hour = event_stop.split(year)[1]
                        events[date].update(
                            {
                                "name": _event.name,
                                "date" : event_start_date,
                                "start": event_start_hour,
                                "stop" : event_stop_hour,
                                "description": _event.description,
                                "location": _event.location
                            }
                        )
        # print(events)
        event_count = len(events)
        return events, event_count


# ─────────────────────────────────────────────────────────────────────────────
