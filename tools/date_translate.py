from datetime import datetime

# French months dictionnary
months = {
    "01": "janvier",
    "02": "février",
    "03": "mars",
    "04": "avril",
    "05": "mai",
    "06": "juin",
    "07": "juillet",
    "08": "août",
    "09": "septembre",
    "10": "octobre",
    "11": "novembre",
    "12": "décembre",
}


# ─── DD/MM/YYYY TO DD MM YY ──────────────────────────────────────────────────


def dmy_to_my(date):
    """
    Translate date format dd-mm-yyyy to day month year
    """
    day = date.split("-")[0]
    month = months[date.split("-")[1]]
    year = date.split("-")[2]

    return f"{day} {month} {year}"


# ─────────────────────────────────────────────────────────────────────────────

# ─── YYYY-MM-DDTHH:MM:SS TO DAY MONTH YEAR HOUR:MINUTES ──────────────────────


def ymdthms_to_dmyhm(date):
    """
    Translate date format YYYY-MM-DDTHH:MM:SS to day num month year hour:minutes
    """
    date = str(date)
    date = datetime.strptime(date, "%Y-%m-%dT%H:%M:%S")
    new_date = date.strftime("%A-%d-%B-%Y-%H:%M")
    day_name = new_date.split("-")[0]
    day_num = new_date.split("-")[1]
    month = new_date.split("-")[2]
    year = new_date.split("-")[3]
    hour = new_date.split("-")[4]

    return f"{day_name} {day_num} {month} {year} {hour}"


# ─────────────────────────────────────────────────────────────────────────────


# ─── TIMESTAMP TO DAY-MONTH-YEAR ─────────────────────────────────────────────


def timestamp_to_dmy(timestamp):
    """
    Translate timestamp date to day-month-year
    """
    date = datetime.fromtimestamp(timestamp)
    new_date = date.strftime("%d-%m-%Y-%H:%M")
    day = new_date.split("-")[0]
    month = new_date.split("-")[1]
    year = new_date.split("-")[2]
    hour = new_date.split("-")[3]

    return f"{day}-{month}-{year}"


# ────────────────────────────────────────────────────────────────────────────────
