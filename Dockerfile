FROM python:3-alpine


####     Install locale for alpine Linux     ####

# set our environment variables
ENV LANG="fr_FR.UTF-8"
ENV LANGUAGE="fr_FR.UTF-8"
ENV LC_ALL=
ENV LC_CTYPE="fr_FR.UTF-8"
ENV LC_COLLATE="fr_FR.UTF-8"
ENV LC_MESSAGES="fr_FR.UTF-8"
ENV LC_MONETARY="fr_FR.UTF-8"
ENV LC_NUMERIC="fr_FR.UTF-8"
ENV LC_TIME="fr_FR.UTF-8"
ENV MUSL_LOCPATH="/usr/share/i18n/locales/musl"

# install libintl
# then install dev dependencies for musl-locales
# clone the sources
# build and install musl-locales
# remove sources and compile artifacts
# lastly remove dev dependencies again
RUN apk update && apk upgrade \
&& apk --no-cache add libintl \
&& apk --no-cache --virtual .locale_build add cmake make musl-dev gcc gettext-dev git \
&& git clone https://gitlab.com/rilian-la-te/musl-locales \
&& cd musl-locales && cmake -DLOCALE_PROFILE=OFF -DCMAKE_INSTALL_PREFIX:PATH=/usr . && make && make install \
&& cd .. && rm -r musl-locales \
&& apk del .locale_build


####    Project installation    ####

# Define working directory
RUN mkdir /opt/MoodleDiscordBot
WORKDIR /opt/MoodleDiscordBot

# Dependencies
RUN apk add gcc build-base libffi-dev chromium chromium-chromedriver

# Copy project files
COPY --chmod=755 bot.py .
ADD Moodle ./Moodle
ADD tools ./tools
ADD requirements.txt .
ADD --chmod=755 start.sh /.

# Python requirements
RUN pip3 install -U pip && pip3 install -r requirements.txt

#ENTRYPOINT ["/bin/sh"]
ENTRYPOINT ["/bin/sh", "/start.sh"]
