#!/bin/bash

if [ -z $USERNAME -a -z $PASSWORD -a -z TOKEN -a -z $CHANNEL_ID ]; then
    echo "You need to set the USERNAME, PASSWORD, TOKEN and CHANNEL_ID environment variables."
    exit 1
fi

if [ -z $USERNAME ]; then
    echo "You need to set the USERNAME environment variable."
    exit 1
fi

if [ -z $PASSWORD ]; then
    echo "You need to set the PASSWORD environment variable."
    exit 1
fi

if [ -z $TOKEN ]; then
    echo "You need to set the TOKEN environment variable."
    exit 1
fi

if [ -z $CHANNEL_ID ]; then
    echo "You need to set the CHANNEL_ID environment variable."
    exit 1
fi

# Exec bot

python3 -u /opt/MoodleDiscordBot/bot.py -u $USERNAME -p $PASSWORD -t $TOKEN -c $CHANNEL_ID