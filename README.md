# The MoodleDiscordBot project

## Introduction

The project `moodleDiscordBot` is a project developed exclusively under [python3](https://fr.wikipedia.org/wiki/Python_(langage)) :snake:.

Its main mission is to fetch information from the [Moodle](https://moodle.com/) platform of the [CNAM Bretagne](https://lecnam.net/) and send it to the user's request via a reserved Discord channel.

The bot can also cross-reference with a calendar [Framagenda](https://framagenda.org).

## Features

- Display of information about a module;
- Display of upcoming courses/webconfs (D1, D+2, D+3, D+5);
- Display of Moodle events (based on the built-in calendar);
- and other features to come.

## Getting started

### 1. Clone the project

```bash
> git clone https://gitlab.com/etienne.le-lay/moodlediscordbot.git
```

### 2. Install the dependencies

#### Python

```bash
> python3 -m venv
> source venv/bin/activate
> python3 -m pip install -r requirements
```

#### Chrome driver

Because of the SSO system set up by the CNAM to connect to Moodle, the script uses `selenium` to retrieve the session cookie. 
And to run `selenium`, I chose to use the `chrome` driver.

- First, install chrome or chromium.
- Check the version:
```bash
> chromium --version
Chromium 96.0.4664.110
```

- Download the corresponding `chromedriver` version available [here](https://chromedriver.chromium.org/downloads).
- Put the file in the root folder of the project and leave the name as is.

### 3. Prepare the bot

- To use the bot, you will first have to create a Discord application [here](https://discord.com/developers/applications).

- Then, you will be provided with an authentication token, necessary to authenticate the bot to the server.

- To get the invitation link, go to the `OAuth2` section. 
  - Choose `In-app Authorization`, 
  - and select the following rights: `Send Messages`, `Read Messages/View channels`, `Embed links`, `Attach files`, `Use External Emojis`, `Use External Stickers`.

It goes without saying that you must have a valid `lecnam.net` account to allow the bot to go and scrape the Moodle.

## Usage

###  Launching the bot

<img src="assets/help_menu.png" alt="help_menu" style="zoom: 50%;" />

#### Normal behavior

<img src="assets/normal_behavior.png" alt="normal behavior" style="zoom:50%;" />

### On the discord side

Available commands :

- `check_api`
- `infos`
- `modules`
- `moodle_calendar`
- `three_days`
- `today`
- `tomorrow`
- `week`

## Docker

To run the the bot into a docker, you can use the `Dockerfile` provided. But you must specify `<user>` `<pass>` `<discord_token` before.

```bash
> docker build -t mdb .
> docker run -dit --name mooodleDiscordBot mdb
```


