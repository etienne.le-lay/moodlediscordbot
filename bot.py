#!/usr/bin/env python3
# *-* coding: utf-8 *-*

# ─── LIBRAIRIES IMPORT ───────────────────────────────────────────────────────

import asyncio
from datetime import datetime
import schedule
import re
import sys
import click
import discord
from discord.ext import commands, tasks
from discord.ext.commands import CommandNotFound
from pretty_help import PrettyHelp
from Moodle import Moodle
from tools import colors
import locale
import os

# ─────────────────────────────────────────────────────────────────────────────
# ─── CLICK OPTIONS ───────────────────────────────────────────────────────────


@click.command()
@click.option(
    "--username",
    "-u",
    required=True,
    type=str,
    help="lecnam.net account e-mail address (e.g., \
        john.doe@auditeur.lecnam.net",
)
@click.option("--password", "-p", required=True, type=str, help="lecnam.net account password")
@click.option("--token", "-t", required=True, type=str, help="Discord bot token")
@click.option("--channel_ids", "-c", required=True, type=str, help="Discord channel(s) ID(s)")

# ─────────────────────────────────────────────────────────────────────────────
# ─── MAIN FUNCTION ───────────────────────────────────────────────────────────
def main(username=None, password=None, token=None, channel_id=None):
    if username is None or password is None or token is None or channel_id is None:
        try:
            username = os.getenv("USERNAME")
            password = os.getenv("PASSWORD")
            token = os.getenv("TOKEN")
            channel_ids = os.getenv("CHANNEL_IDS").split(',')
        except:
            print("Missing arguments"
                  "Please provide username, password and token"
                  "or set them as environment variables"
                  "USERNAME, PASSWORD, TOKEN and CHANNEL_IDS"
                  "respectively")
            sys.exit(1)

    locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')
    today = datetime.today()
    today = today.strftime("%d-%m-%Y")
    timestamp = int(datetime.strptime(str(today), "%d-%m-%Y").timestamp())

    # We check for the syntax of the username
    if username is not None and password is not None:
        uname_regex = re.compile(r"[\d\w\.-]+(auditeur)?(@lecnam\.net)?")
        if not uname_regex.match(username):
            sys.exit(
                f"[{colors.red('!')}] Your e-mail address is not in the right format ! Please check it."
            )
    else:
        sys.exit(f"[{colors.red('!')}] You must enter a username and password")

    if token is None:
        sys.exit("You must enter a token for Discord bot !")

    # We create a Moodle object which consists in a requests.Session()
    # This session will be used fot all requests to Moodle API
    moodle = Moodle.Moodle()
    moodle.connect(username, password)
    check = moodle.check_connection()

    if check != "✅":
        sys.exit(f"[{colors.red('!')}] Couln't connect to Moodle API !")

    courses, dict_name_id = moodle.get_courses()
    print(f"[{colors.blue('*')}] Launching discord bot...")
    
    bot = commands.Bot(command_prefix=commands.when_mentioned_or("!"), help_command=PrettyHelp())
    bot.help_command = PrettyHelp(
        no_category=":hash: Commandes disponibles",
        show_index=False,
        ending_note="Copyright © 2022 - Sysr3ll",
    )

    # ─────────────────────────────────────────────────────────────────────────

    # When discord bot is successfully connected to the Server
    @bot.event
    async def on_ready():

        print(f"[{colors.green('+')}] Ready to receive commands ! 🤖")
        print(f"[{colors.yellow('?')}] I am running on {bot.user.name}")
        print(f"[{colors.yellow('?')}] With the ID: {bot.user.id}")
        print(f"[{colors.yellow('?')}] Creating weekly task")
        # Send weekly reminder
        if datetime.today().weekday() == 0:
            print(f"[{colors.blue('*')}] Sending reminder...")
            for channel_id in channel_ids:
                channel_id = int(channel_id.strip())
                await week(ctx=None, channel_id=channel_id)

    # Avoid bot crash
    @bot.event
    async def on_command_error(ctx, error):
        if isinstance(error, CommandNotFound):
            print(f"[{colors.red('!')}] Command not found")
            return

    # ─────────────────────────────────────────────────────────────────────────
    @bot.command(
        name="modules", help="Affiche la liste des différents modules disponibles."
    )
    async def modules(ctx):
        print(f"[{colors.blue('*')}] Requesting for modules list")
        embed = discord.Embed(
            title="📕 Modules disponibles", color=discord.Color.orange()
        )

        for name, desc in courses.items():
            embed.add_field(name=f"*{name}*", value=desc, inline=False)

        await ctx.send(embed=embed)

    # ─────────────────────────────────────────────────────────────────────────

    @bot.command(name="today", help="Affiche le programme d'aujourd'hui.")
    async def today(ctx):

        embed = discord.Embed(
            title="🗓️ Programme d'aujourd'hui", color=discord.Color.orange()
        )

        frama_events = moodle.get_next_courses(
            day="today")

        embed_desc = ""
        for event, sub in frama_events.items():
            event_date = str(event)
            if len(sub) != 0:
                for sub_id, sub_val in sub.items():
                    event_name = sub_val["name"]
                    event_start = sub_val["start"]
                    event_stop = sub_val["stop"]
                    event_location = f"{sub_val['location']}\n" if sub_val["location"] is not None else "\r"
                    event_description = f"\n{sub_val['description']}\n" if sub_val["description"] is not None else "\r"
                    if ("Gyver" or "Worty" or "Ryptix" or "Noiche" or "Sean") in event_name:
                        event_name += " 🥷🏻"
                    if event_name in ["Distanciel", "Présentiel"]:
                        embed_desc = f"> Début de la semaine en {event_name}\n"
                        continue
                    embed_desc += f"{event_name} de {event_start} à{event_stop}\n{event_description}\n{event_location}\n"

                embed.add_field(
                    name=event_date,
                    value=embed_desc,

                )
            else:
                embed.add_field(
                    name=event_date,
                    value="Temps libre",
                    inline=True,

                )
            embed_desc = ""

        await ctx.send(embed=embed)

    # ─────────────────────────────────────────────────────────────────────────

    @bot.command(name="tomorrow", help="Affiche le programme de demain.")
    async def tomorrow(ctx):

        embed = discord.Embed(
            title="🗓️ Programme de demain", color=discord.Color.orange()
        )

        frama_events = moodle.get_next_courses(
            day="tomorrow")

        embed_desc = ""
        for event, sub in frama_events.items():
            event_date = str(event)
            if len(sub) != 0:
                for sub_id, sub_val in sub.items():
                    event_name = sub_val["name"]
                    event_start = sub_val["start"]
                    event_stop = sub_val["stop"]
                    event_location = f"{sub_val['location']}\n" if sub_val["location"] is not None else "\r"
                    event_description = f"\n{sub_val['description']}\n" if sub_val["description"] is not None else "\r"
                    if ("Gyver" or "Worty" or "Ryptix" or "Noiche" or "Sean") in event_name:
                        event_name += " 🥷🏻"
                    if event_name in ["Distanciel", "Présentiel"]:
                        embed_desc = f"> Début de la semaine en {event_name}\n"
                        continue
                    embed_desc += f"{event_name} de {event_start} à{event_stop}\n{event_description}\n{event_location}\n"

                embed.add_field(
                    name=event_date,
                    value=embed_desc,

                )
            else:
                embed.add_field(
                    name=event_date,
                    value="Temps libre",
                    inline=True,

                )
            embed_desc = ""

        await ctx.send(embed=embed)

    # ─────────────────────────────────────────────────────────────────────────

    @bot.command(name="three_days", help="Affiche le programme sur 3 jours.")
    async def three_days(ctx):
        embed = discord.Embed(
            title="🗓️ Programme des 3 prochains jours", color=discord.Color.orange()
        )

        frama_events = moodle.get_next_courses(
            day="3days")

        embed_desc = ""
        for event, sub in frama_events.items():
            event_date = str(event)
            if len(sub) != 0:
                for sub_id, sub_val in sub.items():
                    event_name = sub_val["name"]
                    event_start = sub_val["start"]
                    event_stop = sub_val["stop"]
                    event_location = f"{sub_val['location']}\n" if sub_val["location"] is not None else "\r"
                    event_description = f"\n{sub_val['description']}\n" if sub_val["description"] is not None else "\r"
                    if ("Gyver" or "Worty" or "Ryptix" or "Noiche" or "Sean") in event_name:
                        event_name += " 🥷🏻"
                    if event_name in ["Distanciel", "Présentiel"]:
                        embed_desc = f"> Début de la semaine en {event_name}\n"
                        continue
                    embed_desc += f"{event_name} de {event_start} à{event_stop}\n{event_description}\n{event_location}\n"

                embed.add_field(
                    name=event_date,
                    value=embed_desc,
                    inline=True,

                )

            else:
                embed.add_field(
                    name=event_date,
                    value="Temps libre",
                    inline=True,

                )
            embed_desc = ""

        await ctx.send(embed=embed)

    # ─────────────────────────────────────────────────────────────────────────

    @bot.command(name="week", help="Affiche le programme sur 5 jours.")
    async def week(ctx=None, channel_id=None):

        embed = discord.Embed(
            title="🗓️ Programme des 5 prochains jours", color=discord.Color.orange()
        )

        frama_events = moodle.get_next_courses(day="week")
        embed_desc = ""
        for event, sub in frama_events.items():
            event_date = str(event)
            if len(sub) != 0:
                for sub_id, sub_val in sub.items():
                    event_name = sub_val["name"]
                    event_start = sub_val["start"]
                    event_stop = sub_val["stop"]
                    event_location = f"{sub_val['location']}\n" if sub_val["location"] is not None else "\r"
                    event_description = f"\n{sub_val['description']}\n" if sub_val["description"] is not None else "\r"
                    if ("Gyver" or "Worty" or "Ryptix" or "Noiche" or "Sean") in (event_name or event_description):
                        event_name += " 🥷🏻"
                    if event_name in ["Distanciel", "Présentiel"]:
                        embed_desc = f"> Début de la semaine en {event_name}\n"
                        continue
                    embed_desc += f"{event_name} de {event_start} à{event_stop}\n{event_description}\n{event_location}\n"

                embed.add_field(
                    name=event_date,
                    value=embed_desc,
                    inline=True,

                )

            else:
                embed.add_field(
                    name=event_date,
                    value="Temps libre",
                    inline=True,

                )

            embed_desc = ""

        if ctx is None and channel_id is not None:
            channel = bot.get_channel(channel_id)
            try:
                await channel.send(embed=embed)
            except:
                pass
        else:
            await ctx.send(embed=embed)

    # ─────────────────────────────────────────────────────────────────────────

    @bot.command(
        name="infos",
        help="""Affiche les informations sur le module choisi.
        Non sensible à la casse (normalement), mais ne mettez pas d'espace SVP :)""",
    )
    async def infos(ctx, arg=None):
        course_name = str(arg)

        if len(re.findall(r"[a-zA-Z_-]+\d+", course_name)) > 1:
            print(f"[{colors.red('!')}] Possible SSTI injection detected !")
            embed = discord.Embed(
                title="Mmmm Worty est-ce toi ?",
                description="Tu n'essaierais pas de ~~tester~~ pirater mon bot par hasard ? 🥷",
                color=discord.Color.darker_gray(),
            )
            await ctx.send(embed=embed)
            return
        else:
            course_name = str(course_name).replace(
                "_", "").replace("-", "").upper()

        if course_name is None or course_name not in dict_name_id:
            embed = discord.Embed(
                title="Oups !",
                description="""Il semblerait que ce module n'existe pas (encore).
                Utilises `!modules` pour afficher la liste des modules disponibles.""",
                color=discord.Color.red(),
            )

            await ctx.send(embed=embed)
            print(f"[{colors.red('!')}] Course {course_name} not found !")
            return

        course_desc = courses[course_name]
        url, events_count, events = moodle.get_course_infos(
            course_name=course_name)
        embed = discord.Embed(
            title=course_name,
            url=url,
            description=course_desc,
            color=discord.Color.orange(),
        )

        embed.add_field(
            name="*Nombre de séances ce mois-ci*", value=events_count, inline=False
        )

        if len(events) == 0:
            to_send = "Aucune séance ce mois-ci"
        else:
            to_send = ""

        for event in events:
            event_start_date = events[event]["start_date"]
            event_start_hour = events[event]["start_hour"]
            event_stop_hour = events[event]["stop_hour"]
            if events[event]["location"] is not None:
                location = events[event]["location"]
            else:
                location = "Non définie"
            to_send += f"{event_start_date} de {event_start_hour} à {event_stop_hour}\nLocalisation: {location}\n\n"

        embed.add_field(name="*Prochaines séances*",
                        value=to_send, inline=False)

        await ctx.send(embed=embed)

    # ──────────────────────────────────────────────────────────────────────────

    @bot.command(name="check_api", help="Vérifie la connexion avec l'API de Moodle.")
    async def check_api(ctx):

        check = moodle.check_connection()

        if check == "✅":
            embed = discord.Embed(
                title="🌐 *Connexion à l'API Moodle*",
                description="It works !",
                color=discord.Color.green(),
            )
        else:
            embed = discord.Embed(
                title="🌐 *Connexion à l'API Moodle*",
                description="404 not found",
                color=discord.Color.red(),
            )

        await ctx.send(embed=embed)

    # ─────────────────────────────────────────────────────────────────────────

    # After all commands definition,
    # we run the bot with its authentication token
    bot.run(token)


if __name__ == "__main__":
    main()
