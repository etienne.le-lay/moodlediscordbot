#!/usr/bin/env python3
# *-* coding: utf-8 *-*

# ─── MODULE INFORMATIONS ─────────────────────────────────────────────────────

# This module is part of the moodleDisordBot project

__author__ = "Etienne Le Lay"
__version__ = 1.2

# ─────────────────────────────────────────────────────────────────────────────

# ─── LIBRARIES IMPORTS ───────────────────────────────────────────────────────

from bs4 import BeautifulSoup as bs4
from datetime import datetime, timedelta
import distro
import os
import re
import requests as req
import requests_random_user_agent

from tools import colors, framagenda
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# ─────────────────────────────────────────────────────────────────────────────

# ─── VARIABLES DEFINITION ────────────────────────────────────────────────────

FRONT_PAGE_URL = "https://brt.moodle.lecnam.net/"
COURSE_VIEW_URL = "https://brt.moodle.lecnam.net/course/view.php?id="
HOW_TO_START_URL = "https://brt.moodle.lecnam.net/course/view.php?id="
courses_names = []
modules_names = []
courses_ids = []
dict_courses = {}
events_ids = []
dict_name_id = {}

# ─────────────────────────────────────────────────────────────────────────────


class Moodle:
    def __init__(self):
        self.session = ""
        pass

    # ─── CONNECT TO MOODLE API THROUGHT LECNAM SSO ───────────────────────────

    def connect(self, username, password):
        """
        Initiate Moddle session with given credentials using chrome driver.
        Usage :
        >> moodle = Moodle()
        """

        s = req.Session()

        # Chrome driver options to avoid the GUI to launch
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument('--disable-dev-shm-usage')

        print(f"[{colors.blue('*')}] Launching browser")
        if distro.linux_distribution()[0] == 'Alpine Linux':
            driver = webdriver.Chrome(options=chrome_options)
        else:
            driver = webdriver.Chrome(
                executable_path=f"{os.getcwd()}/chromedriver",
                options=chrome_options)
        driver.get("https://sts.lecnam.net")

        print(f"[{colors.green('+')}] lecnam.net auth API status: ", end="")
        if "Cnam : authentification" in driver.title:
            print(colors.green("✅"))
        else:
            print(colors.red("❌"))

        # Searching for forms inputs
        uname_input = driver.find_element("name","j_username")
        pw_input = driver.find_element("name","j_password")

        # Sending credentials to the form inputs
        uname_input.send_keys(username)
        pw_input.send_keys(password)

        # We simulate a press on ENTER button to submit the form.
        pw_input.send_keys(Keys.RETURN)

        # We get the last cookies, Moodle's ones
        driver.get(FRONT_PAGE_URL)

        cookies = driver.get_cookies()
        for cookie in cookies:
            req.utils.add_dict_to_cookiejar(
                s.cookies, {cookie["name"]: cookie["value"]}
            )
        self.session = s
        driver.close()

    # ─────────────────────────────────────────────────────────────────────────

    # ─── CHECK CONNECTION TO MOODLE API ──────────────────────────────────────

    def check_connection(self):
        """
        Check if the session is still valid to make requests to Moodle API
        """
        s = self.session
        front_page = s.get(url=FRONT_PAGE_URL).content.decode("utf-8")
        print(f"[{colors.yellow('?')}] Access to Moodle API : ", end="")
        if "AUTHENTIFICATION" in front_page:
            print(f"{colors.red('❌')}")
            check = "❌"
        else:
            print(colors.green("✅"))
            check = "✅"

        return check

    # ─────────────────────────────────────────────────────────────────────────

    # ─── AVAILABLE COURSES ───────────────────────────────────────────────────

    def get_courses(self):
        """
        Returns two dictionnaries with the availabes courses :
        {'module_name' : 'course_name'}
        {'module_name' : 'course_id'}
        """

        s = self.session
        front_page = s.get(url=FRONT_PAGE_URL).content.decode("utf-8")
        soup = bs4(front_page, features="html.parser")

        # Searching for courses names
        searchcourses_names = soup.find_all("span", {"class": "media-body"})

        for result in searchcourses_names:
            if "ESNA" in result.get_text():
                module_name = result.get_text().split(" ")[0]
                course_name = result.get_text().split(":")[1]
                modules_names.append(module_name)
                courses_names.append(course_name)

        # Searching for courses IDs
        search_courses_ids = soup.find_all(
            "a",
            {
                "class": "list-group-item list-group-item-action localboostnavigationcollapsiblechild"
            },
        )
        for result in search_courses_ids:
            course_id = result.get("href").split("id=")[1]

            if (
                len(course_id) > 3
            ):  # There are IDs composed of two digits, they do not refer to courses
                courses_ids.append(course_id)

        for _ in range(len(modules_names)):
            dict_courses[modules_names[_]] = courses_names[_]
            dict_name_id[modules_names[_]] = courses_ids[_]

        return dict_courses, dict_name_id

    # ─────────────────────────────────────────────────────────────────────────

    def get_next_courses(self, day=None):
        """
        Scrapes Moodle courses section2 to retrieve the HowToStart PDF files
        and search for next conference dates. Also search in the framagenda.

        Options:
            day:    Time of days range. {today|tomorow|3days|week} (default: today=0)
        """

        # Args handling
        if day not in ["today", "tomorrow", "3days", "week"]:
            day = "today"
        
        # Date formatting
        today = datetime.today()
        today_str = today.strftime("%d-%m-%Y")
        frama_day = today_str.split("-")[0]
        frama_month = today_str.split("-")[1]
        frama_months = []
        frama_months.append(frama_month)
        
        # If the request is made the 30/01/2022 for example 
        # the events needs to be searched in the next month.
        # So we add a the next month in the list
        if day != "today": 
            next_month = today + timedelta(days=28)
            frama_months.append(next_month.strftime("%m"))
        # print(frama_months)

        frama_months = list(dict.fromkeys(frama_months)) # Remove duplicates
        frama_year = today_str.split("-")[2]

        delta_dict = {"today": 0, "tomorrow": 1, "3days": 2, "week": 4}
        int_delta = delta_dict[day]
        dates = []
        
        # Creating an array of dates based on the delta given
        for d in range(0, int_delta + 1):
            tmp_date = today + timedelta(days=d)
            dates.append(tmp_date.strftime("%A %d %B %Y"))

        if int_delta == 1:  # Exclusive case for 'tomorrow'
            dates = []
            tmp_date = today + timedelta(days=1)
            dates.append(tmp_date.strftime("%A %d %B %Y"))
        print(
            f"[{colors.yellow('?')}] Choice : {day}, which means these dates : {dates}"
        )
        print(
            f"[{colors.blue('*')}] Filtering events for this interval : {dates[0], dates[-1]}"
        )
        # print(frama_months)
        #Get events of the month
        frama_events, event_count = framagenda.get_frama_events(
            frama_months, frama_year
        )
        found_events = {}
        event_id = 0
        for date in dates:
            found_events[date] = {}
            for event in frama_events:
                event_id += 1
                event_date = frama_events[event]["date"]
                if date in event_date:
                    event_name = frama_events[event]["name"]
                    event_start_hour = frama_events[event]["start"]
                    event_stop_hour = frama_events[event]["stop"]
                    event_location = frama_events[event]["location"]
                    event_description = frama_events[event]["description"]
                    found_events[date][event_id] = {
                        "name": event_name,
                        "start": event_start_hour,
                        "stop": event_stop_hour,
                        "description": event_description,
                        "location": event_location,
                        }
        return found_events

    # ─── MOODLE COURSE INFOS ────────────────────────────────────────────────────────

    def get_course_infos(self, course_name=None):
        """
        Regroup informations about a course.
        """

        if course_name is None:
            print(f"[{colors.red('!')}] Please specify the course name.\n")
        elif course_name not in dict_name_id:
            print(
                f"[{colors.red('!')}] Please verify that this course : {course_name} exists"
            )
        
        events, events_count = framagenda.get_frama_events(
            course_name=course_name)
        
        course_id = dict_name_id[course_name]
        course_url = f"{COURSE_VIEW_URL}{course_id}"

        return course_url, events_count, events
